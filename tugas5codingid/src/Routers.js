import React, { useState } from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Navbar from './component/Navbar';
import { Home } from './page/Home';
import { About } from './page/About';
import { Hobby } from './page/Hobby';
import {View} from './page/view';


export const Routers = () => {
    const [datas,setDatas] = useState([]);
  return (
    <Router>
        <Navbar datas={datas}/>
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route exact path="/about">
            <About/>
          </Route>
          <Route exact path="/hobby">
            <Hobby/>
          </Route>
          <Route exact path="/view/:id" component={View}>

          </Route>
        </Switch>
    </Router>
  )
}
