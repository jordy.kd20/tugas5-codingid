import React, {useState} from 'react'
import { AppBar, Box, Button, Grid, Link, Modal, TextField, Toolbar } from '@mui/material';
import "../css/ouser.css";
import { NavLink ,useLocation } from 'react-router-dom';




const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 300,
    bgcolor: 'background.paper',
    border: '60px solid #e4dbdb',
    boxShadow: 24,
    p: 4,
  };


export default function Navbar(props) {
    const pathname = useLocation().pathname
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [Index, setIndex] = useState();
    const [ArrayData, setArrayData] = useState({
        nama: "",
        address: "",
        hobby: "",
        id:0
    });
    const [Submit, setSubmit] = useState(false);
    // console.log(props.datas)

    const prop = props.datas;
    // console.log(prop);

  return (
    <div>
        <Grid container spacing={2}>
        <Box sx={{ flexGrow: 1}}>
            <AppBar position="static">
            <Toolbar>
            <Grid item xl={6} xs={6}>
                <span class={pathname === "/" ? "SelectedButton" : "UnSelectedButton"}>
                    <Button variant='contained' component="div" sx={{ flexGrow: 1 }}>
                        <NavLink to={{pathname:"/",state:{prop}}} exact>Home</NavLink>
                    </Button>
                </span>
                
                <Button variant='contained' component="div" sx={{ flexGrow: 1 }}>
                    <NavLink to={{pathname:"/about",state:{prop}}} exact>About</NavLink>
                </Button>
                <Button variant='contained' component="div" sx={{ flexGrow: 1 }}>
                    <NavLink to={{pathname:"/hobby",state:{prop}}} exact>Hobby</NavLink>
                </Button>
            </Grid>
            <Grid item xl={6} xs={6}>
                <Grid item sx={{textAlign:'end'}}>
                <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px'}} onClick={handleOpen}>Add User</Button>
                </Grid>
            </Grid>
            </Toolbar>
            
            </AppBar>
            
        </Box>
      </Grid>
      <Modal
  open={open}
  onClose={handleClose}
  aria-labelledby="modal-modal-title"
  aria-describedby="modal-modal-description"
>
  <Box sx={style}>
    <Grid className='modaladduser' container>
        <Grid item xl={12} textAlign='center'><h1>Add User</h1></Grid>
        <Grid item xl={12} className='textfield'>
        <TextField  required id='outlined-required' label='Nama' onChange={(e)=>setArrayData({...ArrayData,nama:e.target.value})} ></TextField>
        </Grid>
        <Grid item xl={12} className='textfield'>
        <TextField required id='outlined-required' label='Address' onChange={(e)=>setArrayData({...ArrayData,address:e.target.value})} ></TextField>
        </Grid>
        <Grid item xl={12} className='textfield'>
        <TextField required id='outlined-required' label='Hobby' onChange={(e)=>setArrayData({...ArrayData,hobby:e.target.value})} ></TextField>
        </Grid>
        <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px',width:'100px'}} onClick={()=>{
            setArrayData({...ArrayData,id:prop.length+1,})
            prop.push(ArrayData);
            handleClose();
        }} ><NavLink to={{pathname: "/",state : {prop}}} exact>Save</NavLink>
        </Button>
    </Grid>   
  </Box>
</Modal>
    </div>
  )
}
