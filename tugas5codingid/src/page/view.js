import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from '@mui/material';
import React from 'react';
import { useLocation, useParams } from "react-router-dom";
import picHob from "../asset/img/doodle.jpg"


export const View = () => {
  const location = useLocation();
  const id = useParams();
  if(location.state.ArrayData === undefined){

  }
  const prop = location.state.ArrayData;
  return (
    <div>
      <Grid container m={3} sx={{display:'flex', justifyContent:'space-evenly',alignItems:'center'}}>
      <Card sx={{ Width:'300px' }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="140"
          image={picHob}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h2" component="div">
            {prop[id.id].nama}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <h4>Hobby : {prop[id.id].hobby}</h4>
            <h5>Address : {prop[id.id].address}</h5>
            
          </Typography>
        </CardContent>
      </CardActionArea>
      </Card>
      </Grid>
        
    </div>
  )
}
