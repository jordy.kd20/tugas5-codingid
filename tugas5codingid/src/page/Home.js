import { Box, Button, Grid, Modal, TextField } from '@mui/material'
import React, { useState } from 'react'
import { useLocation } from 'react-router-dom'
import { NavLink } from 'react-router-dom';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 300,
    bgcolor: 'background.paper',
    border: '60px solid #e4dbdb',
    boxShadow: 24,
    p: 4,
  };
export const Home = () => {
    const location = useLocation()
    console.log(location);
    const [Nama, setNama] = useState("");
    const [Addres, setAddres] = useState("");
    const [Hobby, setHobby] = useState("");
    const [openedit, setOpenedit] = React.useState(false);
    const handleOpenedit = () => setOpenedit(true);
    const handleCloseedit = () => setOpenedit(false);
    const [Index, setIndex] = useState();
    
    const [Search, setSearch] = useState("");
    
    if(location.state === undefined){
        return <Grid container direction='column' justifyContent='space-evenly' alignItems='center' >
                <Grid item xl={12} className='texto'>0</Grid>
                <Grid item xl={12} className='textu'>USER</Grid>
            </Grid>
    }
    const ArrayData = location.state.prop;
    console.log(ArrayData);

    const handleuser =()=>{
        let datahandle = ArrayData ? ArrayData : []
        if(datahandle.length===0){
            return <Grid container direction='column' justifyContent='space-evenly' alignItems='center' >
                <Grid item xl={12} className='texto'>0</Grid>
                <Grid item xl={12} className='textu'>USER</Grid>
            </Grid>
        }
        else{
            return datahandle.map((value,index)=>{
                return <div className='boxuser' key={value.id}>
                    <div>
                        <div><h2>{value.nama}</h2></div>
                        <div>{value.address}</div>
                    </div>
                    <div>
                        <h2>{value.hobby}</h2>
                        <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px'}}>
                        <NavLink to={{pathname:`/view/${value.id}`,state:{ArrayData}}}>View</NavLink>
                        </Button>
                        <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px'}} onClick={()=>{
                        setNama(value.nama);
                        setAddres(value.address);
                        setHobby(value.hobby);
                        setIndex(value.id);
                        handleOpenedit();
                    }} >Edit</Button>
                    </div>
                </div>
            })
        }
        
    }
    let datahandle = ArrayData ? ArrayData : [];

    const results = datahandle.filter((x)=>x.nama.toLowerCase().includes(Search.toLowerCase()));
  return (
    <div>
        <Grid item xl={12} xs={12} mt='1rem'>
            <TextField required id='outlined-required' label='Search' onChange={(e)=>{setSearch(e.target.value)}}></TextField>
        </Grid>
        <Grid container spacing={2} xl={12}>
        <Grid item xl={12} xs={12}>
            {Search ? results.map((value,index)=>{
              return(
                <div className='boxuser'>
                <div>
                    <div><h2>{value.nama}</h2></div>
                    <div>{value.address}</div>
                </div>
                <div>
                    <h2>{value.hobby}</h2>
                    <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px'}}>
                        <NavLink to={{pathname:`/view/${value.id}`,state:{ArrayData}}}>View</NavLink>
                    </Button>
                    <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px'}} onClick={()=>{
                        setNama(value.nama);
                        setAddres(value.address);
                        setHobby(value.hobby);
                        setIndex(value.id);
                        handleOpenedit();
                    }} >Edit</Button>
                    
                    
                </div>
              </div>
              ); 
            }) 
            : handleuser()
          }
           
        </Grid>
    </Grid>
    {/* modal edit user */}
<Modal
  open={openedit}
  onClose={handleCloseedit}
  aria-labelledby="modal-modal-title"
  aria-describedby="modal-modal-description"
>
  <Box sx={style}>
    <Grid className='modaledituser' container>
        <Grid item xl={12} textAlign='center'><h1>Edit User</h1></Grid>
        <Grid item xl={12} className='textfield'>
        <TextField  required id='outlined-required' label='Nama' value={Nama} onChange={(e)=>{setNama(e.target.value)}} ></TextField>
        </Grid>
        <Grid item xl={12} className='textfield'>
        <TextField required id='outlined-required' label='Address'  value={Addres} onChange={(e)=>{setAddres(e.target.value)}} ></TextField>
        </Grid>
        <Grid item xl={12} className='textfield'>
        <TextField required id='outlined-required' label='Hobby'  value={Hobby} onChange={(e)=>{setHobby(e.target.value)}} ></TextField>
        </Grid>
        
        <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px',width:'100px'}} onClick={()=>{
          ArrayData[Index].nama = Nama
          ArrayData[Index].address = Addres
          ArrayData[Index].hobby = Hobby
          handleCloseedit()
        }}>
            Save
            </Button>
    </Grid>
    
  </Box>
</Modal> 
    </div> 
  )
}
