import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from '@mui/material';
import React from 'react';
import { useLocation } from "react-router-dom";
import picHobby from "../asset/img/doodle.jpg";

export const Hobby = () => {
  const location = useLocation();
  if(location.state.prop === undefined){
    return <Grid container display='flex' direction='column' justifyContent='space-evenly' alignItems='center' >
    <Grid item xl={12} sx={{fontSize:'100px',color:'#f7b531'}}>0</Grid>
    <Grid item xl={12} sx={{fontSize:'100px',color:'#ff5757'}}>USER</Grid>
    </Grid>
  }
  const ArrayData = location.state.prop;
  const handlehobby =()=>{
    let datahandle = ArrayData ? ArrayData : []
    if(datahandle.length === 0){
      return <Grid container  direction='column' justifyContent='space-evenly' alignItems='center' >
                <Grid item xl={12} sx={{fontSize:'100px',color:'#f7b531'}}>0</Grid>
                <Grid item xl={12} sx={{fontSize:'100px',color:'#ff5757'}}>USER</Grid>
            </Grid>
    }else{
      return datahandle.map((value,index)=>{
        return  <Grid item>
        <Card sx={{ maxWidth: 345 , float:'left', m:3}}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image={picHobby}
                alt="Event"
              />
              <CardContent>
              <Typography gutterBottom variant="h3" component="div">
                {value.hobby}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
      })
    }
  }

  return (
    <div>
      <h1 style={{textAlign:'center'}}>List Hoby</h1>
      <Grid Container spacing={2} sx={{display:'flex' ,float:'Left', marginRight:'10px'}}>
        {handlehobby()}
        
      </Grid>
    </div>
  )
}
